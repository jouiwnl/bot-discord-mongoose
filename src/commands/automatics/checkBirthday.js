import BirthdayRole from '../../model/birthdayrole.js';
import Channel from '../../model/channels.js';
import User from '../../model/users.js';
import buildMessage from '../../embeds/happyBirthday.js';
import getData from '../../utils/data.js';
import logger from '../../logger/index.js';

const checkbirthday = async (client) => {
  logger.info('Iniciando processamento de aniversário para todas as guildas...');
  client.guilds.cache.map(async guild => {

    const birthdayRole = await BirthdayRole.findOne({ guildId: guild.id });
    const channel = await Channel.findOne({ guildId: guild.id });
    const usuarios = await User.find({ guildId: guild.id });

    usuarios.map(async usuario => { 
      if (usuario.birthday == getData()) {
        guild.members.cache.map(item => {
          if(item.id == usuario.userId) {
            const canal = guild.channels.cache.get(channel.id);  
            canal.send({ 
              embeds: [buildMessage(usuario, guild.channels.cache.get(channel.id))] 
            });
            item.roles.add(guild.roles.cache.get(birthdayRole.birthdayRoleId));
          }
        });
      }

      if (usuario.birthday != getData()) {
        guild.members.cache.map(item => {
          if(item.id == usuario.userId && item.roles.cache.get(birthdayRole.birthdayRoleId)) {
            item.roles.remove(guild.roles.cache.get(birthdayRole.birthdayRoleId));
          }
        });
      }
    }); 
  });
  logger.info('Processamento de aniversários concluído.');
};

export default checkbirthday;
