import mongoose from 'mongoose';
import config from '../../configs/config.js';
import logger from '../logger/index.js';

const url = config.baseUrlMongo;

try {
  // Connect to the MongoDB cluster
  mongoose.connect(
    url,
    { useNewUrlParser: true, useUnifiedTopology: true },
    () => logger.info('Conectado com a base de dados'),
  );
} catch (e) {
  logger.error('Houve um erro ao conectar com a base de dados');
}

mongoose.Promise = global.Promise;

export default mongoose;
