import express from 'express';
import logger from '../logger';
const server = express();

server.all('/', (req, res) => {
  res.send('Bot is running');
});  
  
const openServer = () => {
  server.listen(process.env.PORT || 3000, () => {
    logger.info('Servidor rodando');
  });
};

export default openServer;
